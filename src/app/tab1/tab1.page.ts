import { Component } from '@angular/core';
import { PDFGenerator } from '@ionic-native/pdf-generator/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  htmlSample: any;

  constructor(private pdfGenerator: PDFGenerator) {
  }

  /*
  openPdf() {
    var options: DocumentViewerOptions = {
      title: 'My PDF'
    }
    this.document.viewDocument('assets/a4.pdf', 'application/pdf', options)
  }
  */

  getPDF() {
    this.htmlSample = "<html><h1>Converted from HTML</h1></html>";
    let options = {
      documentSize: 'A4',
      type: 'share'
    }
    this.pdfGenerator.fromData(this.htmlSample, options).
      then(resolve => {
        console.log(resolve);

      }
      ).catch((err) => {
        console.error(err);
      });
  }

}
